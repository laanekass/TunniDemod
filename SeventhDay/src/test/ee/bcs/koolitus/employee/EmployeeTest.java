package test.ee.bcs.koolitus.employee;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import ee.bcs.koolitus.employee.Employee;

public class EmployeeTest {

	@Test
	public void testChangeSalryUpdatesSalary() {
		Employee testEmployee = new Employee("Mari", BigDecimal.valueOf(1000));
		testEmployee.changeSalary(testEmployee);
		Assert.assertEquals(BigDecimal.valueOf(1101), testEmployee.getSalary());
	}

}
