package ee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Employee {
	private BigDecimal salary = BigDecimal.ZERO;
	private String name;

	private Employee() {
	}

	public Employee(BigDecimal salary) {
		this.salary = salary;
	}

	public Employee(String name, BigDecimal salary) {
		this.name = name;
		this.salary = salary;
	}

	// getteri tagastustüüpi ei tule muuta
	public BigDecimal getSalary() {
		return salary;
	}

	public Employee setSalary(BigDecimal salary) {
		this.salary = salary;
		return this;
	}

	public String getName() {
		return name;
	}

	public Employee setName(String name) {
		this.name = name;
		return this;
	}

	public void changeSalary(Employee employee) {
		// employee = new Employee();
		// employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100)));
		changeSalary(employee, BigDecimal.valueOf(100));
	}

	public void changeSalary(Employee employee, BigDecimal newSalary) {
		employee.setSalary(employee.getSalary().add(newSalary));
	}

	@Override
	public String toString() {
		return this.name + " has salary " + this.salary;
	}
}
