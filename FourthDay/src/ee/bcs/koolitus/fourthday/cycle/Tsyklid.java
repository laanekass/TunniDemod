package ee.bcs.koolitus.fourthday.cycle;

public class Tsyklid {

	public static void main(String[] args) {
		int i = 5;
		do {
			System.out.println(i++);
		} while (i < 10);

		// i=15;
		int j = 1;
		String kontrollSona = "";
		while (i <= 20 && j < 30 && !kontrollSona.equals("exit")) {
			System.out.println("While ütleb, et i = " + i + " ja j = " + j);
			i = i + 3;
			j = j + 10;

			if (j > 15) {
				//kontrollSona = "exit";
				break;
			}
		}

		String[][] tabel = { { "Mati", "Tallinn" }, { "Kati", "Pärnu" },
				{ "Mari", "Narva" }, { "Jüri", "Türi" } };
		for (int rida = 0; rida < tabel.length; rida++) {
			for (int veerg = 0; veerg < tabel[rida].length; veerg++) {
				System.out.println(tabel[rida][veerg]);
			}
		}

		for (int rida = 0; rida < tabel.length; rida++) {
			System.out.println(tabel[rida][0] + " linnast " + tabel[rida][1]);			
		}

		String sona = "abracadabra";
		for (int taht = 0; taht < sona.length(); taht++) {
			System.out.println(
					(taht + 1) + ". täht on '" + sona.charAt(taht) + "'");
		}
		
		System.out.println("------------while abracadabra---------");
		int taheIndeks = 0;
		while (taheIndeks < sona.length()) {
			System.out.println((taheIndeks + 1) + ". täht on '"
					+ sona.charAt(taheIndeks) + "'");
			taheIndeks++;
		}

	}	
}
