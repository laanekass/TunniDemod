package test.ee.bcs.koolitus.auto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.koolitus.auto.Auto;
import ee.bcs.koolitus.auto.KaiguKast;
import ee.bcs.koolitus.auto.KytuseTyyp;
import ee.bcs.koolitus.auto.Mootor;

public class AutoTest {
	Auto auto;
	
	@Before
	public void setUp(){
		auto = new Auto();
		Mootor mootor = new Mootor();
		mootor.setKaigukast(KaiguKast.AUTOMAAT).setKytuseTyyp(KytuseTyyp.DIISEL).setVoimsus(87);
		auto.setKohtadeArv(2).setMootor(mootor).setUsteArv(3);
	}
	
	@Test
	public void testNotStartedAfterCreation(){
		Assert.assertEquals(false, auto.isMootorKaib());			
	}

	@Test
	public void testDiiselMootoriKaivitamine() {
		auto.kaivitaMootor();
		Assert.assertEquals(true, auto.isMootorKaib());
	}
	
	@Test
	public void testBensiiniMootoriKaivitamine() {
		auto.getMootor().setKytuseTyyp(KytuseTyyp.BENSIIN);
		auto.kaivitaMootor();
		Assert.assertEquals(true, auto.isMootorKaib());
	}

}
