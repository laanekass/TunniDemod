package ee.bcs.koolitus.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try {
			FileWriter writer = new FileWriter(new File("minuUusFail.txt"), true);
			int i = 1;
			while (i <= 5) {
				writer.append("This is a " + i + ". sentence \n");
				i++;
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try (FileWriter writer = new FileWriter(new File("minuUusFail.txt"), true)) {
			int i = 1;
			while (i <= 5) {
				writer.append("This is a " + i + ". sentence \n");
				i++;
			}
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}

}
