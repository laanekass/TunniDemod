package ee.bcs.koolitus.restdemo.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ee.bcs.koolitus.restdemo.bean.Continent;
import ee.bcs.koolitus.restdemo.bean.Country;

public class CountryService {
	static HashMap<Integer, Country> countryIdMap = getCountryIdMap();

	public CountryService() {
	}

	public List<Country> getAllCountries() {
		List<Country> listOfCountries = new ArrayList<>();

		ContinentService continentService = new ContinentService();
		List<Continent> continents = continentService.getAllContinents();

		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM country_management.country;");) {
			while (resultSet.next()) {
				Country country = new Country(resultSet.getInt("country_id"),
						resultSet.getString("name"), resultSet.getLong("population"));
				for (Continent continent : continents) {
					if (continent.getId() == resultSet.getInt("continent_id")) {
						country.setContinent(continent);
					}
				}
				listOfCountries.add(country);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfCountries;
	}

	public Country getCountry(int id) {
		Country country = countryIdMap.get(id);
		return country;
	}

	public Country addCountry(Country country) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO country_management.country "
					+ "(name, population, continent_id) VALUES ('" + country.getCountryName()
					+ "', " + country.getPopulation() + ", " + country.getContinent().getId()
					+ ");");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return country;
	}

	public Country updateCountry(Country country) {
		if (country.getId() <= 0)
			return null;
		countryIdMap.put(country.getId(), country);
		return country;

	}

	public void deleteCountry(int id) {
		countryIdMap.remove(id);
	}

	public static HashMap<Integer, Country> getCountryIdMap() {
		return countryIdMap;
	}

}
