package ee.bcs.koolitus.sixthDay;

/**
 * 
 * @author heleen
 *
 */
public class BeautifulClass {
	private String beautyMeaning;
	private int measure;

	/**
	 * Default
	 */
	public BeautifulClass() {
		this.beautyMeaning = "beautySoul";
		this.measure = 8;
	}

	/**
	 * For creating beautiful Classes objects
	 * 
	 * @param beautyMeaning-
	 *            how beauty is defined
	 * @param measure
	 *            - how it is measured
	 */
	public BeautifulClass(String beautyMeaning, int measure) {
		this.beautyMeaning = beautyMeaning;
		this.measure = measure;
	}

	/**
	 * 
	 * @param beautyMeaning-
	 *            how beauty is defined
	 */
	public BeautifulClass(Integer beautyMeaning) {
		this.beautyMeaning = beautyMeaning.toString();
	}

	/**
	 * 
	 * @param beautyMeaning
	 *            - how beauty is defined
	 */
	public BeautifulClass(Double beautyMeaning) {
		this.beautyMeaning = beautyMeaning.toString();
	}

	/**
	 * This method is for printing to console
	 */
	public void printBeautyLevel(int customMultiplier1) {
		System.out.println("CustomMultiplier  = " + customMultiplier1);
		System.out.println("BeautyLevel is: " + calculateBeautyLevel(customMultiplier1));
	}

	public int calculateBeautyLevel(int customMultiplier2) {
		return measure * customMultiplier2;
	}

	public String getBeautyMeaning() {
		return this.beautyMeaning;
	}

	public void setBeautyMeaning(String beautyMeaning) {
		this.beautyMeaning = beautyMeaning;
	}

	public int getMeasure() {
		return measure;
	}

	public void setMeasure(int measure) {
		this.measure = measure;
	}

	public void setMeasure(String measure) {
		setMeasure(Integer.parseInt(measure));
	}
}
