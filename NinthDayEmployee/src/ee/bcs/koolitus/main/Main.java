package ee.bcs.koolitus.main;

import java.math.BigDecimal;
import java.util.List;

import ee.bcs.koolitus.employee.Employee;
import ee.bcs.koolitus.employee.Manager;
import ee.bcs.koolitus.employee.Person;

public class Main {

	public static void main(String[] args) {
		Person p = new Person(18);
		Employee employee = new Employee(20, "Mari", BigDecimal.valueOf(800));

		System.out.println(p.getAge());
		System.out.println(employee.getName() + " is " + employee.getAge()
				+ " years old and has salary " + employee.getSalary());

		Manager manager = new Manager(35, "Liisa", BigDecimal.valueOf(2000), "accounting");
		System.out.println(manager.getName() + " manages " + manager.getDepartement());

		Employee employee2 = new Employee(45, "Kristjan", BigDecimal.valueOf(1500));

		manager.addEmployeeToDepartement(employee);
		manager.addEmployeeToDepartement(employee2);

		System.out.println("In her departement there are:");
		List<Employee> managerDepartementEmployees = manager.getDepartemnetStaff();
		for (Employee emp : managerDepartementEmployees) {
			System.out.println(emp.getName());
		}

		System.out.println("p id = " + p.getId());
		System.out.println(employee.getName() + " id = " + employee.getId());
		System.out.println(manager.getName() + " id = " + manager.getId());
		System.out.println(employee2.getName() + " id = " + employee2.getId());
	}

}
